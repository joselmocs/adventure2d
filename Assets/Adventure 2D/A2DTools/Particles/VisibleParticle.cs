using System.Collections;
using UnityEngine;


namespace Adventure2D.A2DTools
{
    /// <summary>
    /// Adds this class to particles to force their sorting layer
    /// </summary>
    public class VisibleParticle : MonoBehaviour
    {

        /// <summary>
        /// Sets the particle system's renderer to the Visible Particles sorting layer
        /// </summary>
        protected virtual void Start()
        {
            GetComponent<ParticleSystem>().GetComponent<Renderer>().sortingLayerName = "VisibleParticles";
        }
    }
}
