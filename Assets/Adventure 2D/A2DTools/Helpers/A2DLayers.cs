﻿using UnityEngine;
using System.Collections;

namespace Adventure2D.A2DTools
{
    public class A2DLayers
    {
        public static bool LayerInLayerMask(int layer, LayerMask layerMask)
        {
            if (((1 << layer) & layerMask) != 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

    }
}