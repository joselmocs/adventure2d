﻿#if UNITY_EDITOR
using System;
using UnityEditor;
using Adventure2D;

namespace Adventure2D
{
    public class PackageExporter
    {
        [MenuItem("Adventure 2D/Generate Unity Package")]
        static void CreateUnityPackage()
        {
            var assetPathNames = new string[] {
                "Assets/Adventure 2D"
            };
            var packageName = "Packages/Adventure2D.unitypackage";
            AssetDatabase.ExportPackage(assetPathNames, packageName, ExportPackageOptions.Recurse);
        }
    }
}
#endif