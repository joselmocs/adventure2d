﻿using UnityEngine;
using System.Collections;
using Adventure2D.A2DTools;
using UnityEditor;


namespace Adventure2D.A2DTools
{
    /// <summary>
    /// This class adds a Adventure2D entry in Unity's top menu, allowing to enable/disable the help texts from the engine's inspectors
    /// </summary>
    public static class Adventure2DMenuHelp
    {
        [MenuItem("Adventure 2D/Enable Help in Inspectors", false, 0)]
        /// <summary>
        /// Adds a menu item to enable help
        /// </summary>
        private static void EnableHelpInInspectors()
        {
            SetHelpEnabled(true);
        }

        [MenuItem("Adventure 2D/Enable Help in Inspectors", true)]
        /// <summary>
        /// Conditional method to determine if the "enable help" entry should be greyed or not
        /// </summary>
        private static bool EnableHelpInInspectorsValidation()
        {
            return !HelpEnabled();
        }

        [MenuItem("Adventure 2D/Disable Help in Inspectors", false, 1)]
        /// <summary>
        /// Adds a menu item to disable help
        /// </summary>
        private static void DisableHelpInInspectors()
        {
            SetHelpEnabled(false);
        }

        [MenuItem("Adventure 2D/Disable Help in Inspectors", true)]
        /// <summary>
        /// Conditional method to determine if the "disable help" entry should be greyed or not
        /// </summary>
        private static bool DisableHelpInInspectorsValidation()
        {
            return HelpEnabled();
        }

        /// <summary>
        /// Checks editor prefs to see if help is enabled or not
        /// </summary>
        /// <returns><c>true</c>, if enabled was helped, <c>false</c> otherwise.</returns>
        private static bool HelpEnabled()
        {
            if (EditorPrefs.HasKey("A2DShowHelpInInspectors"))
            {
                return EditorPrefs.GetBool("A2DShowHelpInInspectors");
            }
            else
            {
                EditorPrefs.SetBool("A2DShowHelpInInspectors", true);
                return true;
            }
        }

        /// <summary>
        /// Sets the help enabled editor pref.
        /// </summary>
        /// <param name="status">If set to <c>true</c> status.</param>
        private static void SetHelpEnabled(bool status)
        {
            EditorPrefs.SetBool("A2DShowHelpInInspectors", status);
            SceneView.RepaintAll();
        }
    }
}