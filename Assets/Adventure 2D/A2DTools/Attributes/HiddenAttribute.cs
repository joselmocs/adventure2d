﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Adventure2D.A2DTools
{
    public class HiddenAttribute : PropertyAttribute { }
}
