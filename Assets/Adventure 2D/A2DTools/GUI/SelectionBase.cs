using UnityEngine;


namespace Adventure2D.A2DTools
{
    [SelectionBase]
    /// <summary>
    /// Add this component to an object and it'll always get selection in scene view, even if you select one of its children
    /// </summary>
    public class SelectionBase : MonoBehaviour
    {

    }
}
