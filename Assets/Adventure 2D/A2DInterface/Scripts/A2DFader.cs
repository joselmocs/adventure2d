﻿using UnityEngine;
using UnityEngine.UI;
using Adventure2D.A2DTools;


namespace Adventure2D.A2DInterface
{
    /// <summary>
    /// Events used to trigger faders on or off
    /// </summary>
    public struct A2DFadeEvent
    {
        public float Duration;
        public float TargetAlpha;

        /// <summary>
        /// Initializes a new instance of the <see cref="Adventure2D.A2DInterface.A2DFadeEvent"/> struct.
        /// </summary>
        /// <param name="duration">Duration, in seconds.</param>
        /// <param name="targetAlpha">Target alpha, from 0 to 1.</param>
        public A2DFadeEvent (float duration, float targetAlpha)
        {
            Duration = duration;
            TargetAlpha = targetAlpha;
        }
    }

    public struct A2DFadeInEvent
    {
        public float Duration;

        /// <summary>
        /// Initializes a new instance of the <see cref="Adventure2D.A2DInterface.A2DFadeInEvent"/> struct.
        /// </summary>
        /// <param name="duration">Duration.</param>
        public A2DFadeInEvent(float duration)
        {
            Duration = duration;
        }
    }

    public struct A2DFadeOutEvent
    {
        public float Duration;

        /// <summary>
        /// Initializes a new instance of the <see cref="Adventure2D.A2DInterface.A2DFadeOutEvent"/> struct.
        /// </summary>
        /// <param name="duration">Duration.</param>
        public A2DFadeOutEvent(float duration)
        {
            Duration = duration;
        }
    }

    /// <summary>
    /// The Fader class can be put on an Image, and it'll intercept A2DFadeEvents and turn itself on or off accordingly.
    /// </summary>
    public class A2DFader : MonoBehaviour, A2DEventListener<A2DFadeEvent>, A2DEventListener<A2DFadeInEvent>, A2DEventListener<A2DFadeOutEvent>
    {
        /// the default duration of the fade in/out
        public float DefaultDuration = 0.2f;
        /// whether or not the fade should happen in unscaled time
        public bool Unscaled = true;

        /// the alpha the fader should have when active (usually 1)
        public float FaderActiveAlpha = 1f;
        /// the alpha the fader should have when inactive (usually 0)
        public float FaderInactiveAlpha = 0f;

        protected CanvasGroup _canvasGroup;
        protected Image _image;

        protected float _currentTargetAlpha;
        protected float _currentDuration;

        protected bool _fading = false;
        protected float _fadeDirection = 1f;
        protected float _fadeCounter;

        /// <summary>
        /// On Start, we initialize our fader
        /// </summary>
        protected virtual void Start()
        {
            Initialization ();
        }

        /// <summary>
        /// On init, we grab our components, and disable/hide everything
        /// </summary>
        protected virtual void Initialization()
        {
            _canvasGroup = GetComponent<CanvasGroup> ();
            _canvasGroup.alpha = 0;

            _image = GetComponent<Image> ();
            _image.enabled = false;
        }

        /// <summary>
        /// On Update, we update our alpha
        /// </summary>
        protected virtual void Update()
        {
            if (_canvasGroup == null) { return; }

            if ((_fading) && (_canvasGroup.alpha != _currentTargetAlpha))
            {
                if (_fadeCounter < 1f)
                {
                    EnableFader ();

                    _canvasGroup.alpha = Mathf.SmoothStep(_canvasGroup.alpha,_currentTargetAlpha,_fadeCounter);

                    if (Unscaled)
                    {
                        _fadeCounter += Time.unscaledDeltaTime / _currentDuration;
                    }
                    else
                    {
                        _fadeCounter += Time.deltaTime / _currentDuration;
                    }
                }
                else
                {
                    StopFading ();
                }
            }
            else
            {
                StopFading ();
            }
        }

        /// <summary>
        /// Stops the fading.
        /// </summary>
        protected virtual void StopFading()
        {
            _canvasGroup.alpha = _currentTargetAlpha;
            _fading = false;
            if (_canvasGroup.alpha == 0)
            {
                DisableFader ();
            }
        }

        /// <summary>
        /// Disables the fader.
        /// </summary>
        protected virtual void DisableFader()
        {
            _image.enabled = false;
            _canvasGroup.blocksRaycasts = false;
        }

        /// <summary>
        /// Enables the fader.
        /// </summary>
        protected virtual void EnableFader()
        {
            _image.enabled = true;
            _canvasGroup.blocksRaycasts = true;
        }

        /// <summary>
        /// When catching a fade event, we fade our image in or out
        /// </summary>
        /// <param name="fadeEvent">Fade event.</param>
        public virtual void OnA2DEvent(A2DFadeEvent fadeEvent)
        {
            _fading = true;
            _fadeCounter = 0f;
            _currentTargetAlpha = (fadeEvent.TargetAlpha == -1) ? FaderActiveAlpha : fadeEvent.TargetAlpha;
            _currentDuration = fadeEvent.Duration;
        }

        /// <summary>
        /// When catching an A2DFadeInEvent, we fade our image in
        /// </summary>
        /// <param name="fadeEvent">Fade event.</param>
        public virtual void OnA2DEvent(A2DFadeInEvent fadeEvent)
        {
            _fading = true;
            _fadeCounter = 0f;
            _currentTargetAlpha = FaderActiveAlpha;
            _currentDuration = DefaultDuration;
        }

        /// <summary>
        /// When catching an A2DFadeOutEvent, we fade our image out
        /// </summary>
        /// <param name="fadeEvent">Fade event.</param>
        public virtual void OnA2DEvent(A2DFadeOutEvent fadeEvent)
        {
            _fading = true;
            _fadeCounter = 0f;
            _currentTargetAlpha = FaderInactiveAlpha;
            _currentDuration = DefaultDuration;
        }

        /// <summary>
        /// On enable, we start listening to events
        /// </summary>
        protected virtual void OnEnable()
        {
            this.A2DEventStartListening<A2DFadeEvent> ();
            this.A2DEventStartListening<A2DFadeInEvent> ();
            this.A2DEventStartListening<A2DFadeOutEvent> ();
        }

        /// <summary>
        /// On disable, we stop listening to events
        /// </summary>
        protected virtual void OnDisable()
        {
            this.A2DEventStopListening<A2DFadeEvent> ();
            this.A2DEventStopListening<A2DFadeInEvent> ();
            this.A2DEventStopListening<A2DFadeOutEvent> ();
        }
    }
}
