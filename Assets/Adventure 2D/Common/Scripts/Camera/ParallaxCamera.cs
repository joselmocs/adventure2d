﻿using System.Collections;
using UnityEngine;
using Adventure2D.A2DTools;


namespace Adventure2D.Engine
{
	/// <summary>
	/// Add this class to a camera to have it support parallax layers
	/// </summary>
	[AddComponentMenu("Adventure2D/Camera/Parallax Camera")]
	public class ParallaxCamera : MonoBehaviour
	{
		[Information("If you set MoveParallax to true, the camera movement will cause parallax elements to move accordingly.",Adventure2D.A2DTools.InformationAttribute.InformationType.Info,false)]
		public bool MoveParallax=true;
	}
}