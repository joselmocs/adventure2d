﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Adventure2D.A2DTools;


namespace Adventure2D.Engine
{
    /// <summary>
    /// This persistent singleton handles the inputs and sends commands to the player.
    /// IMPORTANT : this script's Execution Order MUST be -100.
    /// You can define a script's execution order by clicking on the script's file and then clicking on the Execution Order button at the bottom right of the script's inspector.
    /// See https://docs.unity3d.com/Manual/class-ScriptExecution.html for more details
    /// </summary>
    [AddComponentMenu("Adventure2D/Managers/Input Manager")]
    public class InputManager : Singleton<InputManager>
    {
        /// the possible kinds of control used for movement
        [Header("Movement settings")]
        [Information("Turn SmoothMovement on to have inertia in your controls (meaning there'll be a small delay between a press/release of a direction and your character moving/stopping). You can also define here the horizontal and vertical thresholds.", InformationAttribute.InformationType.Info, false)]
        /// If set to true, acceleration / deceleration will take place when moving / stopping
        public bool SmoothMovement = true;
        /// the minimum horizontal and vertical value you need to reach to trigger movement on an analog controller (joystick for example)
        public Vector2 Threshold = new Vector2(0.1f, 0.4f);

        /// the jump button, used for jumps and validation
        public A2DInput.IButton JumpButton { get; protected set; }
        /// the jetpack button
        public A2DInput.IButton JetpackButton { get; protected set; }
        /// the run button
        public A2DInput.IButton RunButton { get; protected set; }
        /// the dash button
        public A2DInput.IButton DashButton { get; protected set; }
        /// the shoot button
        public A2DInput.IButton ShootButton { get; protected set; }
        /// the reload button
        public A2DInput.IButton ReloadButton { get; protected set; }
        /// the pause button
        public A2DInput.IButton PauseButton { get; protected set; }
        /// the switch weapon button
        public A2DInput.IButton SwitchWeaponButton { get; protected set; }
        /// the shoot axis, used as a button (non analogic)
        public A2DInput.ButtonStates ShootAxis { get; protected set; }
        /// the primary movement value (used to move the character around)
        public Vector2 PrimaryMovement { get { return _primaryMovement; } }
        /// the secondary movement (usually the right stick on a gamepad), used to aim
        protected List<A2DInput.IButton> ButtonList;
        protected Vector2 _primaryMovement = Vector2.zero;
        protected string _axisHorizontal;
        protected string _axisVertical;
        protected string _axisShoot;

        /// <summary>
        /// On Start we look for what mode to use, and initialize our axis and buttons
        /// </summary>
        protected virtual void Start()
        {
            InitializeButtons();
            InitializeAxis();
        }

        /// <summary>
        /// Initializes the buttons. If you want to add more buttons, make sure to register them here.
        /// </summary>
        protected virtual void InitializeButtons()
        {
            ButtonList = new List<A2DInput.IButton>();
            ButtonList.Add(JumpButton = new A2DInput.IButton("Jump", JumpButtonDown, JumpButtonPressed, JumpButtonUp));
            //ButtonList.Add(JetpackButton = new A2DInput.IButton("Jetpack", JetpackButtonDown, JetpackButtonPressed, JetpackButtonUp));
            ButtonList.Add(RunButton = new A2DInput.IButton("Run", RunButtonDown, RunButtonPressed, RunButtonUp));
            ButtonList.Add(DashButton = new A2DInput.IButton("Dash", DashButtonDown, DashButtonPressed, DashButtonUp));
            //ButtonList.Add(ShootButton = new A2DInput.IButton("Shoot", ShootButtonDown, ShootButtonPressed, ShootButtonUp));
            //ButtonList.Add(ReloadButton = new A2DInput.IButton("Reload", ReloadButtonDown, ReloadButtonPressed, ReloadButtonUp));
            //ButtonList.Add(SwitchWeaponButton = new A2DInput.IButton("SwitchWeapon", SwitchWeaponButtonDown, SwitchWeaponButtonPressed, SwitchWeaponButtonUp));
            ButtonList.Add(PauseButton = new A2DInput.IButton("Pause", PauseButtonDown, PauseButtonPressed, PauseButtonUp));
        }

        /// <summary>
        /// Initializes the axis strings.
        /// </summary>
        protected virtual void InitializeAxis()
        {
            _axisHorizontal = "Horizontal";
            _axisVertical = "Vertical";
            _axisShoot = "ShootAxis";
        }

        /// <summary>
        /// On LateUpdate, we process our button states
        /// </summary>
        protected virtual void LateUpdate()
        {
            ProcessButtonStates();
        }

        /// <summary>
        /// At update, we check the various commands and update our values and states accordingly.
        /// </summary>
        protected virtual void Update()
        {
            SetMovement();
            SetShootAxis();
            GetInputButtons();
        }

        /// <summary>
        /// If we're not on mobile, watches for input changes, and updates our buttons states accordingly
        /// </summary>
        protected virtual void GetInputButtons()
        {
            foreach (A2DInput.IButton button in ButtonList)
            {
                if (Input.GetButton(button.ButtonID))
                {
                    button.TriggerButtonPressed();
                }
                if (Input.GetButtonDown(button.ButtonID))
                {
                    button.TriggerButtonDown();
                }
                if (Input.GetButtonUp(button.ButtonID))
                {
                    button.TriggerButtonUp();
                }
            }
        }

        /// <summary>
        /// Called at LateUpdate(), this method processes the button states of all registered buttons
        /// </summary>
        public virtual void ProcessButtonStates()
        {
            // for each button, if we were at ButtonDown this frame, we go to ButtonPressed. If we were at ButtonUp, we're now Off
            foreach (A2DInput.IButton button in ButtonList)
            {
                if (button.State.CurrentState == A2DInput.ButtonStates.ButtonDown)
                {
                    button.State.ChangeState(A2DInput.ButtonStates.ButtonPressed);
                }
                if (button.State.CurrentState == A2DInput.ButtonStates.ButtonUp)
                {
                    button.State.ChangeState(A2DInput.ButtonStates.Off);
                }
            }
        }

        /// <summary>
        /// Called every frame, gets primary movement values from input
        /// </summary>
        public virtual void SetMovement()
        {
            if (SmoothMovement)
            {
                _primaryMovement.x = Input.GetAxis(_axisHorizontal);
                _primaryMovement.y = Input.GetAxis(_axisVertical);
            }
            else
            {
                _primaryMovement.x = Input.GetAxisRaw(_axisHorizontal);
                _primaryMovement.y = Input.GetAxisRaw(_axisVertical);
            }
        }

        /// <summary>
        /// Called every frame, gets shoot axis values from input
        /// </summary>
        protected virtual void SetShootAxis()
        {
            ShootAxis = A2DInput.ProcessAxisAsButton(_axisShoot, Threshold.y, ShootAxis);
        }

        public virtual void JumpButtonDown() { JumpButton.State.ChangeState(A2DInput.ButtonStates.ButtonDown); }
        public virtual void JumpButtonPressed() { JumpButton.State.ChangeState(A2DInput.ButtonStates.ButtonPressed); }
        public virtual void JumpButtonUp() { JumpButton.State.ChangeState(A2DInput.ButtonStates.ButtonUp); }

        public virtual void DashButtonDown() { DashButton.State.ChangeState(A2DInput.ButtonStates.ButtonDown); }
        public virtual void DashButtonPressed() { DashButton.State.ChangeState(A2DInput.ButtonStates.ButtonPressed); }
        public virtual void DashButtonUp() { DashButton.State.ChangeState(A2DInput.ButtonStates.ButtonUp); }

        public virtual void RunButtonDown() { RunButton.State.ChangeState(A2DInput.ButtonStates.ButtonDown); }
        public virtual void RunButtonPressed() { RunButton.State.ChangeState(A2DInput.ButtonStates.ButtonPressed); }
        public virtual void RunButtonUp() { RunButton.State.ChangeState(A2DInput.ButtonStates.ButtonUp); }

        public virtual void JetpackButtonDown() { JetpackButton.State.ChangeState(A2DInput.ButtonStates.ButtonDown); }
        public virtual void JetpackButtonPressed() { JetpackButton.State.ChangeState(A2DInput.ButtonStates.ButtonPressed); }
        public virtual void JetpackButtonUp() { JetpackButton.State.ChangeState(A2DInput.ButtonStates.ButtonUp); }

        public virtual void ReloadButtonDown() { ReloadButton.State.ChangeState(A2DInput.ButtonStates.ButtonDown); }
        public virtual void ReloadButtonPressed() { ReloadButton.State.ChangeState(A2DInput.ButtonStates.ButtonPressed); }
        public virtual void ReloadButtonUp() { ReloadButton.State.ChangeState(A2DInput.ButtonStates.ButtonUp); }

        public virtual void ShootButtonDown() { ShootButton.State.ChangeState(A2DInput.ButtonStates.ButtonDown); }
        public virtual void ShootButtonPressed() { ShootButton.State.ChangeState(A2DInput.ButtonStates.ButtonPressed); }
        public virtual void ShootButtonUp() { ShootButton.State.ChangeState(A2DInput.ButtonStates.ButtonUp); }

        public virtual void PauseButtonDown() { PauseButton.State.ChangeState(A2DInput.ButtonStates.ButtonDown); }
        public virtual void PauseButtonPressed() { PauseButton.State.ChangeState(A2DInput.ButtonStates.ButtonPressed); }
        public virtual void PauseButtonUp() { PauseButton.State.ChangeState(A2DInput.ButtonStates.ButtonUp); }

        public virtual void SwitchWeaponButtonDown() { SwitchWeaponButton.State.ChangeState(A2DInput.ButtonStates.ButtonDown); }
        public virtual void SwitchWeaponButtonPressed() { SwitchWeaponButton.State.ChangeState(A2DInput.ButtonStates.ButtonPressed); }
        public virtual void SwitchWeaponButtonUp() { SwitchWeaponButton.State.ChangeState(A2DInput.ButtonStates.ButtonUp); }

    }
}