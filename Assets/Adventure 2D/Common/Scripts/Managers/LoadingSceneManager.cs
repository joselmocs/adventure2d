﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Adventure2D.A2DTools;
using Adventure2D.A2DInterface;


namespace Adventure2D.Engine
{
    /// <summary>
    /// A class to load scenes using a loading screen instead of just the default API
    /// </summary>
    public class LoadingSceneManager : MonoBehaviour
    {
        [Header("Binding")]
        /// The name of the scene to load while the actual target scene is loading (usually a loading screen)
        public static string LoadingScreenSceneName = "LoadingScreen";

        [Header("GameObjects")]
        /// the canvas group containing the progress bar
        public CanvasGroup LoadingProgressBar;

        [Header("Time")]
        /// the duration (in seconds) of the initial fade in
        public float StartFadeDuration = 0.3f;
        /// the speed of the progress bar
        public float ProgressBarSpeed = 2f;
        /// the duration (in seconds) of the load complete fade out
        public float ExitFadeDuration = 0.3f;
        /// the delay (in seconds) before leaving the scene when complete
        public float LoadCompleteDelay = 0.3f;

        protected AsyncOperation _asyncOperation;
        protected static string _sceneToLoad = "";
        protected float _fillTarget = 0f;

        /// <summary>
        /// Call this static method to load a scene from anywhere
        /// </summary>
        /// <param name="sceneToLoad">Level name.</param>
        public static void LoadScene(string sceneToLoad)
        {
            _sceneToLoad = sceneToLoad;
            Application.backgroundLoadingPriority = ThreadPriority.High;
            if (LoadingScreenSceneName != null)
            {
                SceneManager.LoadScene(LoadingScreenSceneName);
            }
        }

        /// <summary>
        /// On Start(), we start loading the new level asynchronously
        /// </summary>
        protected virtual void Start()
        {
            if (_sceneToLoad != "")
            {
                StartCoroutine(LoadAsynchronously());
            }
        }

        /// <summary>
        /// Every frame, we fill the bar smoothly according to loading progress
        /// </summary>
        protected virtual void Update()
        {
            LoadingProgressBar.GetComponent<Image>().fillAmount = A2DMaths.Approach(LoadingProgressBar.GetComponent<Image>().fillAmount, _fillTarget, Time.deltaTime * ProgressBarSpeed);
        }

        /// <summary>
        /// Loads the scene to load asynchronously.
        /// </summary>
        protected virtual IEnumerator LoadAsynchronously()
        {
            // we setup our various visual elements
            LoadingSetup();

            // we start loading the scene
            _asyncOperation = SceneManager.LoadSceneAsync(_sceneToLoad, LoadSceneMode.Single);
            _asyncOperation.allowSceneActivation = false;

            // while the scene loads, we assign its progress to a target that we'll use to fill the progress bar smoothly
            while (_asyncOperation.progress < 0.9f)
            {
                _fillTarget = _asyncOperation.progress;
                yield return null;
            }
            // when the load is close to the end (it'll never reach it), we set it to 100%
            _fillTarget = 1f;

            // we wait for the bar to be visually filled to continue
            while (LoadingProgressBar.GetComponent<Image>().fillAmount != _fillTarget)
            {
                yield return null;
            }

            // the load is now complete, we replace the bar with the complete animation
            yield return new WaitForSeconds(LoadCompleteDelay);

            // we fade to black
            A2DEventManager.TriggerEvent(new A2DFadeInEvent(ExitFadeDuration));
            yield return new WaitForSeconds(ExitFadeDuration);

            // we switch to the new scene
            _asyncOperation.allowSceneActivation = true;
        }

        /// <summary>
        /// Sets up all visual elements, fades from black at the start
        /// </summary>
        protected virtual void LoadingSetup()
        {
            A2DEventManager.TriggerEvent(new A2DFadeOutEvent(StartFadeDuration));

            LoadingProgressBar.GetComponent<Image>().fillAmount = 0f;
        }
    }
}