﻿using UnityEngine;
using System.Collections;
using Adventure2D.A2DTools;
using System.Collections.Generic;


namespace Adventure2D.Engine
{
    /// <summary>
    /// A list of the possible Adventure Engine base events
    /// </summary>
    public enum AdventureEngineEventTypes
    {
        LevelStart,
        LevelComplete,
        LevelEnd,
        Pause,
        UnPause,
        PlayerDeath,
        Respawn,
        GameOver
    }

    /// <summary>
    /// A type of events used to signal level start and end (for now)
    /// </summary>
    public struct AdventureEngineEvent
    {
        public AdventureEngineEventTypes EventType;
        /// <summary>
        /// Initializes a new instance of the <see cref="Adventure2D.Engine.AdventureEngineEvent"/> struct.
        /// </summary>
        /// <param name="eventType">Event type.</param>
        public AdventureEngineEvent(AdventureEngineEventTypes eventType)
        {
            EventType = eventType;
        }
    }

    public class PointsOfEntryStorage
    {
        public string LevelName;
        public int PointOfEntryIndex;
        public Character.FacingDirections FacingDirection;

        public PointsOfEntryStorage(string levelName, int pointOfEntryIndex, Character.FacingDirections facingDirection)
        {
            LevelName = levelName;
            FacingDirection = facingDirection;
            PointOfEntryIndex = pointOfEntryIndex;
        }
    }

    /// <summary>
    /// The game manager is a persistent singleton that handles points and time
    /// </summary>
    [AddComponentMenu("Adventure2D/Managers/Game Manager")]
    public class GameManager : PersistentSingleton<GameManager>,
                                A2DEventListener<AdventureEngineEvent>
    {
        [Header("Settings")]
        /// the target frame rate for the game
        public int TargetFrameRate = 300;

        public string GameOverScene;

        /// true if the game is currently paused
        public bool Paused { get; set; }
        // true if we've stored a map position at least once
        public bool StoredLevelMapPosition { get; set; }
        /// the current player
        public Vector2 LevelMapPosition { get; set; }
        /// the list of points of entry and exit
        public List<PointsOfEntryStorage> PointsOfEntry { get; set; }

        // storage
        protected Stack<float> _savedTimeScale;

        protected override void Awake()
        {
            base.Awake();
            PointsOfEntry = new List<PointsOfEntryStorage>();
        }

        /// <summary>
        /// On Start(), sets the target framerate to whatever's been specified
        /// </summary>
        protected virtual void Start()
        {
            Application.targetFrameRate = TargetFrameRate;
            _savedTimeScale = new Stack<float>();
        }

        /// <summary>
        /// this method resets the whole game manager
        /// </summary>
        public virtual void Reset()
        {
            Time.timeScale = 1f;
            Paused = false;
            PointsOfEntry.Clear();
        }

        /// <summary>
        /// sets the timescale to the one in parameters
        /// </summary>
        /// <param name="newTimeScale">New time scale.</param>
        public virtual void SetTimeScale(float newTimeScale)
        {
            _savedTimeScale.Push(Time.timeScale);
            Time.timeScale = newTimeScale;
        }

        /// <summary>
        /// Resets the time scale to the last saved time scale.
        /// </summary>
        public virtual void ResetTimeScale()
        {
            if (_savedTimeScale.Count > 0)
            {
                Time.timeScale = _savedTimeScale.Peek();
                _savedTimeScale.Pop();
            }
            else
            {
                Time.timeScale = 1f;
            }
        }

        /// <summary>
        /// Pauses the game or unpauses it depending on the current state
        /// </summary>
        public virtual void Pause()
        {
            Instance.SetTimeScale(0.0f);
            Instance.Paused = true;

            if (GUIManager.Instance != null)
            {
                GUIManager.Instance.SetPause(true);
            }
        }

        /// <summary>
        /// Unpauses the game
        /// </summary>
        public virtual void UnPause()
        {
            Instance.ResetTimeScale();
            Instance.Paused = false;

            if (GUIManager.Instance != null)
            {
                GUIManager.Instance.SetPause(false);
            }
        }

        /// <summary>
        /// Deletes all save files
        /// </summary>
        public virtual void ResetAllSaves()
        {
            SaveLoadManager.DeleteSaveFolder("Adventure2D");
        }

        /// <summary>
        /// Stores the points of entry for the level whose name you pass as a parameter.
        /// </summary>
        /// <param name="levelName">Level name.</param>
        /// <param name="entryIndex">Entry index.</param>
        /// <param name="exitIndex">Exit index.</param>
        public virtual void StorePointsOfEntry(string levelName, int entryIndex, Character.FacingDirections facingDirection)
        {
            if (PointsOfEntry.Count > 0)
            {
                foreach (PointsOfEntryStorage point in PointsOfEntry)
                {
                    if (point.LevelName == levelName)
                    {
                        point.PointOfEntryIndex = entryIndex;
                        return;
                    }
                }
            }

            PointsOfEntry.Add(new PointsOfEntryStorage(levelName, entryIndex, facingDirection));
        }

        /// <summary>
        /// Gets point of entry info for the level whose scene name you pass as a parameter
        /// </summary>
        /// <returns>The points of entry.</returns>
        /// <param name="levelName">Level name.</param>
        public virtual PointsOfEntryStorage GetPointsOfEntry(string levelName)
        {
            if (PointsOfEntry.Count > 0)
            {
                foreach (PointsOfEntryStorage point in PointsOfEntry)
                {
                    if (point.LevelName == levelName)
                    {
                        return point;
                    }
                }
            }
            return null;
        }

        /// <summary>
        /// Clears the stored point of entry infos for the level whose name you pass as a parameter
        /// </summary>
        /// <param name="levelName">Level name.</param>
        public virtual void ClearPointOfEntry(string levelName)
        {
            if (PointsOfEntry.Count > 0)
            {
                foreach (PointsOfEntryStorage point in PointsOfEntry)
                {
                    if (point.LevelName == levelName)
                    {
                        PointsOfEntry.Remove(point);
                    }
                }
            }
        }

        /// <summary>
        /// Clears all points of entry.
        /// </summary>
        public virtual void ClearAllPointsOfEntry()
        {
            PointsOfEntry.Clear();
        }

        /// <summary>
        /// Catches AdventureEngineEvents and acts on them, playing the corresponding sounds
        /// </summary>
        /// <param name="engineEvent">AdventureEngineEvent event.</param>
        public virtual void OnA2DEvent(AdventureEngineEvent engineEvent)
        {
            switch (engineEvent.EventType)
            {
                case AdventureEngineEventTypes.Pause:
                    Pause();
                    break;

                case AdventureEngineEventTypes.UnPause:
                    UnPause();
                    break;
            }
        }

        /// <summary>
        /// OnDisable, we start listening to events.
        /// </summary>
        protected virtual void OnEnable()
        {
            this.A2DEventStartListening<AdventureEngineEvent>();
        }

        /// <summary>
        /// OnDisable, we stop listening to events.
        /// </summary>
        protected virtual void OnDisable()
        {
            this.A2DEventStopListening<AdventureEngineEvent>();
        }
    }
}