﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using Adventure2D.A2DTools;
using Adventure2D.A2DInterface;


namespace Adventure2D.Engine
{
    /// <summary>
    /// Spawns the player, handles checkpoints and respawn
    /// </summary>
    [AddComponentMenu("Adventure2D/Managers/Level Manager")]
    public class LevelManager : Singleton<LevelManager>
    {
        /// the possible checkpoint axis
        public enum CheckpointsAxis { x, y, z }
        public enum CheckpointDirections { Ascending, Descending }

        /// the prefab you want for your player
        [Header("Playable Character")]
        [Information("The LevelManager is responsible for handling spawn/respawn, checkpoints management and level bounds. Here you can define the playable characters for your level..", InformationAttribute.InformationType.Info, false)]
        /// the player prefab to instantiate
        public Character PlayerPrefab;

        [Header("Checkpoints")]
        [Information("Here you can select a checkpoint attribution axis (if your level is horizontal go for X, Y if it's vertical), and a debug spawn where your player character will spawn from while in editor mode.", InformationAttribute.InformationType.Info, false)]
        /// Debug spawn
        public CheckPoint DebugSpawn;
        /// the axis on which objects should be compared
        public CheckpointsAxis CheckpointAttributionAxis = CheckpointsAxis.x;

        public CheckpointDirections CheckpointAttributionDirection = CheckpointDirections.Ascending;

        [ReadOnly]
        /// the current checkpoint
        public CheckPoint CurrentCheckPoint;

        [Space(10)]
        [Header("Points of Entry")]
        public Transform[] PointsOfEntry;

        [Space(10)]
        [Header("Intro and Outro durations")]
        [Information("Here you can specify the length of the fade in and fade out at the start and end of your level. You can also determine the delay before a respawn.", InformationAttribute.InformationType.Info, false)]
        /// duration of the initial fade in (in seconds)
        public float IntroFadeDuration = 1f;
        /// duration of the fade to black at the end of the level (in seconds)
        public float OutroFadeDuration = 1f;
        /// duration between a death of the main character and its respawn
        public float RespawnDelay = 2f;


        [Space(10)]
        [Header("Level Bounds")]
        [Information("The level bounds are used to constrain the camera's movement, as well as the player character's. You can see it in real time in the scene view as you adjust its size (it's the yellow box).", InformationAttribute.InformationType.Info, false)]
        /// the level limits, camera and player won't go beyond this point.
        public Bounds LevelBounds = new Bounds(Vector3.zero, Vector3.one * 10);

        /// the elapsed time since the start of the level
        public TimeSpan RunningTime { get { return DateTime.UtcNow - _started; } }
        public CameraController LevelCameraController { get; set; }

        // private stuff
        public Character Player { get; protected set; }
        public List<CheckPoint> Checkpoints { get; protected set; }
        protected DateTime _started;
        protected string _nextLevel = null;

        /// <summary>
        /// On awake, instantiates the player
        /// </summary>
        protected override void Awake()
        {
            base.Awake();
            InstantiatePlayableCharacter();
        }

        /// <summary>
        /// Instantiate playable character based on the one specified in the LevelManager's inspector.
        /// </summary>
        protected virtual void InstantiatePlayableCharacter()
        {
            if (PlayerPrefab == null)
            {
                return;
            }

            // player instantiation
            Character newPlayer = (Character)Instantiate(PlayerPrefab, new Vector3(0, 0, 0), Quaternion.identity);
            newPlayer.name = PlayerPrefab.name;
            Player = newPlayer;

            if (PlayerPrefab.CharacterType != Character.CharacterTypes.Player)
            {
                Debug.LogWarning("LevelManager : The Character you've set in the LevelManager isn't a Player, which means it's probably not going to move. You can change that in the Character component of your prefab.");
            }
        }

        /// <summary>
        /// Initialization
        /// </summary>
        public virtual void Start()
        {
            if (Player == null)
            {
                return;
            }

            Initialization();
            LevelGUIStart();

            // we handle the spawn of the character
            SpawnSingleCharacter();
            CheckpointAssignment();

            // we trigger a level start event
            A2DEventManager.TriggerEvent(new AdventureEngineEvent(AdventureEngineEventTypes.LevelStart));
            A2DEventManager.TriggerEvent(new A2DGameEvent("Load"));

        }

        /// <summary>
        /// Gets current camera, start time, etc.
        /// </summary>
        protected virtual void Initialization()
        {
            // storage
            LevelCameraController = FindObjectOfType<CameraController>();
            _started = DateTime.UtcNow;

            // we store all the checkpoints present in the level, ordered by their x value
            if ((CheckpointAttributionAxis == CheckpointsAxis.x) && (CheckpointAttributionDirection == CheckpointDirections.Ascending))
            {
                Checkpoints = FindObjectsOfType<CheckPoint>().OrderBy(o => o.transform.position.x).ToList();
            }
            if ((CheckpointAttributionAxis == CheckpointsAxis.x) && (CheckpointAttributionDirection == CheckpointDirections.Descending))
            {
                Checkpoints = FindObjectsOfType<CheckPoint>().OrderByDescending(o => o.transform.position.x).ToList();
            }
            if ((CheckpointAttributionAxis == CheckpointsAxis.y) && (CheckpointAttributionDirection == CheckpointDirections.Ascending))
            {
                Checkpoints = FindObjectsOfType<CheckPoint>().OrderBy(o => o.transform.position.y).ToList();
            }
            if ((CheckpointAttributionAxis == CheckpointsAxis.y) && (CheckpointAttributionDirection == CheckpointDirections.Descending))
            {
                Checkpoints = FindObjectsOfType<CheckPoint>().OrderByDescending(o => o.transform.position.y).ToList();
            }
            if ((CheckpointAttributionAxis == CheckpointsAxis.z) && (CheckpointAttributionDirection == CheckpointDirections.Ascending))
            {
                Checkpoints = FindObjectsOfType<CheckPoint>().OrderBy(o => o.transform.position.z).ToList();
            }
            if ((CheckpointAttributionAxis == CheckpointsAxis.z) && (CheckpointAttributionDirection == CheckpointDirections.Descending))
            {
                Checkpoints = FindObjectsOfType<CheckPoint>().OrderByDescending(o => o.transform.position.z).ToList();
            }

            // we assign the first checkpoint
            CurrentCheckPoint = Checkpoints.Count > 0 ? Checkpoints[0] : null;
        }

        /// <summary>
        /// Assigns all respawnable objects in the scene to their checkpoint
        /// </summary>
        protected virtual void CheckpointAssignment()
        {
            // we get all respawnable objects in the scene and attribute them to their corresponding checkpoint
            IEnumerable<Respawnable> listeners = FindObjectsOfType<MonoBehaviour>().OfType<Respawnable>();
            foreach (Respawnable listener in listeners)
            {
                for (int i = Checkpoints.Count - 1; i >= 0; i--)
                {
                    Vector3 vectorDistance = ((MonoBehaviour)listener).transform.position - Checkpoints[i].transform.position;

                    float distance = 0;
                    if (CheckpointAttributionAxis == CheckpointsAxis.x)
                    {
                        distance = vectorDistance.x;
                    }
                    if (CheckpointAttributionAxis == CheckpointsAxis.y)
                    {
                        distance = vectorDistance.y;
                    }
                    if (CheckpointAttributionAxis == CheckpointsAxis.z)
                    {
                        distance = vectorDistance.z;
                    }

                    // if the object is behind the checkpoint (on the attribution axis), we move on to the next checkpoint
                    if ((distance < 0) && (CheckpointAttributionDirection == CheckpointDirections.Ascending))
                    {
                        continue;
                    }
                    if ((distance > 0) && (CheckpointAttributionDirection == CheckpointDirections.Descending))
                    {
                        continue;
                    }

                    // if the object is further on the attribution axis compared to the checkpoint, we assign it to the checkpoint, and proceed to the next object
                    Checkpoints[i].AssignObjectToCheckPoint(listener);
                    break;
                }
            }
        }

        /// <summary>
        /// Initializes GUI stuff
        /// </summary>
        protected virtual void LevelGUIStart()
        {
            // if we have a GUI manager
            if (GUIManager.Instance != null)
            {
                // fade in
                A2DEventManager.TriggerEvent(new A2DFadeOutEvent(IntroFadeDuration));
            }
        }

        /// <summary>
        /// Spawns a playable character into the scene
        /// </summary>
        protected virtual void SpawnSingleCharacter()
        {
            // in debug mode we spawn the player on the debug spawn point
#if UNITY_EDITOR
            if (DebugSpawn != null)
            {
                DebugSpawn.SpawnPlayer(Player);
                return;
            }
#endif
            RegularSpawnSingleCharacter();
        }

        /// <summary>
        /// Spawns the character at the selected entry point if there's one, or at the selected checkpoint.
        /// </summary>
        protected virtual void RegularSpawnSingleCharacter()
        {
            PointsOfEntryStorage point = GameManager.Instance.GetPointsOfEntry(SceneManager.GetActiveScene().name);
            if (point != null)
            {
                Player.RespawnAt(PointsOfEntry[point.PointOfEntryIndex], Character.FacingDirections.Right);
                return;
            }

            if (CurrentCheckPoint != null)
            {
                CurrentCheckPoint.SpawnPlayer(Player);
                return;
            }
        }

        /// <summary>
        /// Every frame we check for checkpoint reach
        /// </summary>
        public virtual void Update()
        {
            if (Player == null)
            {
                return;
            }

            _started = DateTime.UtcNow;
        }

        /// <summary>
        /// Sets the current checkpoint.
        /// </summary>
        /// <param name="newCheckPoint">New check point.</param>
        public virtual void SetCurrentCheckpoint(CheckPoint newCheckPoint)
        {
            CurrentCheckPoint = newCheckPoint;
        }

        public virtual void SetNextLevel(string levelName)
        {
            _nextLevel = levelName;
        }

        public virtual void GotoNextLevel()
        {
            GotoLevel(_nextLevel);
            _nextLevel = null;
        }

        /// <summary>
        /// Gets the player to the specified level
        /// </summary>
        /// <param name="levelName">Level name.</param>
        public virtual void GotoLevel(string levelName)
        {
            A2DEventManager.TriggerEvent(new AdventureEngineEvent(AdventureEngineEventTypes.LevelEnd));
            A2DEventManager.TriggerEvent(new A2DGameEvent("Save"));

            if (GUIManager.Instance != null)
            {
                A2DEventManager.TriggerEvent(new A2DFadeInEvent(OutroFadeDuration));
            }
            StartCoroutine(GotoLevelCo(levelName));
        }

        /// <summary>
        /// Waits for a short time and then loads the specified level
        /// </summary>
        /// <returns>The level co.</returns>
        /// <param name="levelName">Level name.</param>
        protected virtual IEnumerator GotoLevelCo(string levelName)
        {
            if (Player != null)
            {
                Player.Disable();
            }

            if (Time.timeScale > 0.0f)
            {
                yield return new WaitForSeconds(OutroFadeDuration);
            }
            // we trigger an unPause event for the GameManager (and potentially other classes)
            A2DEventManager.TriggerEvent(new AdventureEngineEvent(AdventureEngineEventTypes.UnPause));

            if (string.IsNullOrEmpty(levelName))
            {
                LoadingSceneManager.LoadScene("StartScreen");
            }
            else
            {
                LoadingSceneManager.LoadScene(levelName);
            }
        }

        /// <summary>
        /// Kills the player.
        /// </summary>
        public virtual void KillPlayer(Character player)
        {
            Health characterHealth = player.GetComponent<Health>();
            if (characterHealth == null)
            {
                return;
            }
            else
            {
                // we kill the character
                characterHealth.Kill();
                A2DEventManager.TriggerEvent(new AdventureEngineEvent(AdventureEngineEventTypes.PlayerDeath));

                // if we have only one player, we restart the level
                StartCoroutine(SoloModeRestart());
            }
        }

        /// <summary>
        /// Coroutine that kills the player, stops the camera, resets the points.
        /// </summary>
        /// <returns>The player co.</returns>
        protected virtual IEnumerator SoloModeRestart()
        {
            if (PlayerPrefab == null)
            {
                yield break;
            }

            A2DEventManager.TriggerEvent(new AdventureEngineEvent(AdventureEngineEventTypes.GameOver));
            if ((GameManager.Instance.GameOverScene != null) && (GameManager.Instance.GameOverScene != ""))
            {
                LoadingSceneManager.LoadScene(GameManager.Instance.GameOverScene);
            }

            if (LevelCameraController != null)
            {
                LevelCameraController.FollowsPlayer = false;
            }

            yield return new WaitForSeconds(RespawnDelay);

            if (LevelCameraController != null)
            {
                LevelCameraController.FollowsPlayer = true;
            }

            if (CurrentCheckPoint != null)
            {
                CurrentCheckPoint.SpawnPlayer(Player);
            }
            _started = DateTime.UtcNow;

            // we trigger a respawn event
            A2DEventManager.TriggerEvent(new AdventureEngineEvent(AdventureEngineEventTypes.Respawn));
        }
    }
}