﻿using System.Collections;
using UnityEngine;
using UnityEditor;
using Adventure2D.A2DTools;


namespace Adventure2D.Engine
{
    /// <summary>
    /// Adds items to the Adventure 2D/Help menu
    /// </summary>
    public static class EngineHelpMenu
    {
        [MenuItem("Adventure 2D/Help/Asset's page", false, 50)]
        /// <summary>
        /// Adds a asset's page link to the Adventure 2D/Help menu
        /// </summary>
        private static void OpenAssetsPage()
        {
            Application.OpenURL("http://corgi-engine.moremountains.com/");
        }

        [MenuItem("Adventure 2D/Help/Documentation", false, 50)]
        /// <summary>
        /// Adds a documentation link to the Adventure 2D/Help menu
        /// </summary>
        private static void OpenDocumentation()
        {
            Application.OpenURL("http://corgi-engine-docs.moremountains.com/");
        }

        [MenuItem("Adventure 2D/Help/API Documentation", false, 51)]
        /// <summary>
        /// Adds an API documentation link to the Adventure 2D/Help menu
        /// </summary>
        private static void OpenAPIDocumentation()
        {
            Application.OpenURL("http://corgi-engine-docs.moremountains.com/API/");
        }

        [MenuItem("Adventure 2D/Help/Video Tutorials", false, 50)]
        /// <summary>
        /// Adds a youtube link to the Adventure 2D/Help menu
        /// </summary>
        private static void OpenVideoTutorials()
        {
            Application.OpenURL("https://www.youtube.com/playlist?list=PLl3caEhMYxQEsA5Fbg0M2aB9Q9Z9BTVNS");
        }
    }
}
