﻿using System.Collections;
using UnityEngine;
using Adventure2D.A2DTools;


namespace Adventure2D.Engine
{
	/// <summary>
	/// A list of possible events used by the character
	/// </summary>
	public enum A2DCharacterEventTypes
	{
		ButtonActivation,
		Jump
	}

	/// <summary>
	/// A2DCharacterEvents are used in addition to the events triggered by the character's state machine, to signal stuff happening that is not necessarily linked to a change of state
	/// </summary>
	public struct A2DCharacterEvent
	{
		public Character TargetCharacter;
		public A2DCharacterEventTypes EventType;
		/// <summary>
		/// Initializes a new instance of the <see cref="Adventure2D.Engine.A2DCharacterEvent"/> struct.
		/// </summary>
		/// <param name="character">Character.</param>
		/// <param name="eventType">Event type.</param>
		public A2DCharacterEvent(Character character, A2DCharacterEventTypes eventType)
		{
			TargetCharacter = character;
			EventType = eventType;
		}
	}

	/// <summary>
	/// An event fired when something takes damage
	/// </summary>
	public struct A2DDamageTakenEvent
	{
		public Character AffectedCharacter;
		public GameObject Instigator;
		public float CurrentHealth;
		public float DamageCaused;
		public float PreviousHealth;

		/// <summary>
		/// Initializes a new instance of the <see cref="Adventure2D.Engine.A2DDamageTakenEvent"/> struct.
		/// </summary>
		/// <param name="affectedCharacter">Affected character.</param>
		/// <param name="instigator">Instigator.</param>
		/// <param name="currentHealth">Current health.</param>
		/// <param name="damageCaused">Damage caused.</param>
		/// <param name="previousHealth">Previous health.</param>
		public A2DDamageTakenEvent(Character affectedCharacter, GameObject instigator, float currentHealth, float damageCaused, float previousHealth)
		{
			AffectedCharacter = affectedCharacter;
			Instigator = instigator;
			CurrentHealth = currentHealth;
			DamageCaused = damageCaused;
			PreviousHealth = previousHealth;
		}
	}
}