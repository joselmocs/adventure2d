using System.Collections;
using UnityEngine;
using Adventure2D.A2DTools;


namespace Adventure2D.Engine
{
    /// <summary>
    /// A simple component meant to be added to the pause button
    /// </summary>
    public class PauseButton : MonoBehaviour
    {
        /// Puts the game on pause
        public virtual void PauseButtonAction()
        {
            // we trigger a Pause event for the GameManager and other classes that could be listening to it too
            A2DEventManager.TriggerEvent(new AdventureEngineEvent(AdventureEngineEventTypes.Pause));
        }

        /// Resume the game
        public virtual void UnPauseButtonAction()
        {
            // we trigger a Pause event for the GameManager and other classes that could be listening to it too
            A2DEventManager.TriggerEvent(new AdventureEngineEvent(AdventureEngineEventTypes.UnPause));
        }
    }
}