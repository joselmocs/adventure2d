﻿using UnityEngine;
using Adventure2D.A2DTools;
using UnityEngine.EventSystems;


namespace Adventure2D.Engine
{
    /// <summary>
    /// Handles all GUI effects and changes
    /// </summary>
    public class GUIManager : Singleton<GUIManager>
    {
        /// the game object that contains the heads up display (avatar, health, points...)
        public GameObject HUD;
        /// the pause screen game object
        public GameObject PauseScreen;

        /// the health bar
        public ProgressBar HealthBar;

        /// <summary>
        /// Initialization
        /// </summary>
        protected override void Awake()
        {
            base.Awake();
        }

        /// <summary>
        /// Initialization
        /// </summary>
        protected virtual void Start()
        {

        }

        /// <summary>
        /// Sets the HUD active or inactive
        /// </summary>
        /// <param name="state">If set to <c>true</c> turns the HUD active, turns it off otherwise.</param>
        public virtual void SetHUDActive(bool state)
        {
            if (HUD!= null)
            {
                HUD.SetActive(state);
            }
        }

        /// <summary>
        /// Sets the pause.
        /// </summary>
        /// <param name="state">If set to <c>true</c>, sets the pause.</param>
        public virtual void SetPause(bool state)
        {
            if (PauseScreen != null)
            {
                PauseScreen.SetActive(state);
                EventSystem.current.sendNavigationEvents = true;
            }
        }

        /// <summary>
        /// Updates the health bar.
        /// </summary>
        /// <param name="currentHealth">Current health.</param>
        /// <param name="minHealth">Minimum health.</param>
        /// <param name="maxHealth">Max health.</param>
        /// <param name="playerID">Player I.</param>
        public virtual void UpdateHealthBar(float currentHealth, float minHealth, float maxHealth)
        {
            if (HealthBar == null) {
                return;
            }

            HealthBar.UpdateBar(currentHealth, minHealth, maxHealth);
        }
    }
}