using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using Adventure2D.A2DTools;
using Adventure2D.A2DInterface;

namespace Adventure2D.Engine
{
    /// <summary>
    /// Simple start screen class.
    /// </summary>
    public class StartScreen : MonoBehaviour
    {
        /// the level to load after the start screen
        public string NextLevel;
        /// the delay after which the level should auto skip (if less than 1s, won't autoskip)
        public float AutoSkipDelay = 0f;

        [Header("Fades")]
        public float FadeInDuration = 1f;
        public float FadeOutDuration = 1f;

        [Header("Sound Settings Bindings")]
        public A2DSwitch MusicSwitch;
        public A2DSwitch SfxSwitch;

        /// <summary>
        /// Initialization
        /// </summary>
        protected virtual void Awake()
        {
            //GUIManager.Instance.SetHUDActive(false);
            A2DEventManager.TriggerEvent(new A2DFadeOutEvent(FadeInDuration));

            if (AutoSkipDelay > 1f)
            {
                FadeOutDuration = AutoSkipDelay;
                StartCoroutine(LoadFirstLevel());
            }
        }

        protected virtual void Start()
        {
            if (MusicSwitch != null)
            {
                MusicSwitch.CurrentSwitchState = SoundManager.Instance.Settings.MusicOn ? A2DSwitch.SwitchStates.Right : A2DSwitch.SwitchStates.Left;
                MusicSwitch.InitializeState();
            }

            if (SfxSwitch != null)
            {
                SfxSwitch.CurrentSwitchState = SoundManager.Instance.Settings.SfxOn ? A2DSwitch.SwitchStates.Right : A2DSwitch.SwitchStates.Left;
                SfxSwitch.InitializeState();
            }
        }

        /// <summary>
        /// During update we simply wait for the user to press the "jump" button.
        /// </summary>
        protected virtual void Update()
        {
            if (!Input.GetButtonDown("Jump"))
                return;

            ButtonPressed();
        }

        /// <summary>
        /// What happens when the main button is pressed
        /// </summary>
        public virtual void ButtonPressed()
        {
            A2DEventManager.TriggerEvent(new A2DFadeInEvent(FadeOutDuration));
            // if the user presses the "Jump" button, we start the first level.
            StartCoroutine(LoadFirstLevel());
        }

        /// <summary>
        /// Loads the next level.
        /// </summary>
        /// <returns>The first level.</returns>
        protected virtual IEnumerator LoadFirstLevel()
        {
            yield return new WaitForSeconds(FadeOutDuration);
            LoadingSceneManager.LoadScene(NextLevel);
        }
    }
}