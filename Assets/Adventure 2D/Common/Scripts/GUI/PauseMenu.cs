using System.Collections;
using UnityEngine;
using Adventure2D.A2DTools;


namespace Adventure2D.Engine
{
    public class PauseMenu : MonoBehaviour
    {
        GameManager _gameManager;
        InputManager _inputManager;

        void Start()
        {
            _gameManager = GameManager.Instance;
            _inputManager = InputManager.Instance;
        }

        void Update()
        {
            if (_inputManager.PauseButton.State.CurrentState == A2DInput.ButtonStates.ButtonDown)
            {
                TriggerPause();
            }
        }

        protected virtual void TriggerPause()
        {
            if (_gameManager.Paused) {
                A2DEventManager.TriggerEvent(new AdventureEngineEvent(AdventureEngineEventTypes.UnPause));
            }
            else {
                A2DEventManager.TriggerEvent(new AdventureEngineEvent(AdventureEngineEventTypes.Pause));
            }
        }
    }
}