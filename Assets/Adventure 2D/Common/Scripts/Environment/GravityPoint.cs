﻿using UnityEngine;

namespace Adventure2D.Engine
{
    [AddComponentMenu("Adventure2D/Environment/Gravity Point")]
    public class GravityPoint : MonoBehaviour
    {
        public float DistanceOfEffect;

        protected virtual void OnDrawGizmos()
        {
            Gizmos.color = Color.cyan;
            Gizmos.DrawWireSphere(this.transform.position, DistanceOfEffect);
        }

    }
}
